# pe_master_bootstrap

## Overview

Simple shell script for bootstrapping a PE Puppet master based on a role.

This assumes there's a "role" class for the master that the master can be
classified with.

It will install Git and r10k via the PE gem.
It will clone the control repository, run r10k against the Puppetfile to make
the modules available for bootstrapping, and do a "puppet apply -e 'include...'"
to apply the role.

## Contributions

Yes, please.

But this shouldn't become extremely complicated or require a bunch of
dependencies.
