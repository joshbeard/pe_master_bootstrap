#!/bin/sh
## Bootstrap for PE master
## Installs Git and r10k.  Clones a control repo, installs the modules
## from its Puppetfile, and does a Puppet apply against the master role.

## Set this to the address (git or https) of your control repository.
## If you choose to use ssh protocol, you'll need to have the keys on the
## master and the pubkey on the git server.
CONTROL_SOURCE="http://"

MASTER_ROLE="role::puppet::master"

## A directory we can use to work in for bootstrap
TEMP_DIR="/tmp/puppet_bootstrap"

if [ "$(whoami)" != "root" ]; then
  echo "You must be root to run this."
  exit 1
fi

if [ ! -x "/opt/puppet/bin/puppet" ]; then
  echo "/opt/puppet/bin/puppet doesn't exit."
  exit 1
fi

for i in "$@"
do
  case $i in
    -d|--delete-temp*)
      DELETE_TEMP=true
      shift
      ;;
    -r|--role*)
      MASTER_ROLE="${i#*=}"
      shift
      ;;
    -t|--temp-dir*)
      TEMP_DIR="${i#*=}"
      shift
      ;;
    -s|--control-source*)
      CONTROL_SOURCE="${i#*=}"
      shift
      ;;
    -h|--help*)
      # unknown option
      echo "Usage: $0 [options]"
      echo
      echo "Options:"
      echo "  -d, --delete-temp     Force removal of the temporary directory if it exists"
      echo "  -r, --role            The master role to include for the Puppet apply"
      echo "  -t, --temp-dir        Full path to a temp directory to use"
      echo "  -s, --control-source  URL to the control repository in VCS"
      echo
      exit 0
      ;;
    *)
      echo "Unknown option: ${i}"
      exit 1
      ;;
  esac
done

## Install some needed software
/opt/puppet/bin/puppet resource package git ensure=present || \
  (echo "git failed to install; exiting." && exit 1)

/opt/puppet/bin/gem install r10k || \
  (echo "r10k failed to install; exiting" && exit 1)

## Clone the control repo.  Ensure the path exists first.
_basedir=$(dirname "${TEMP_DIR}")

if [ -d "${TEMP_DIR}" ]; then
  if [ ! -z "${DELETE_TEMP}" ]; then
    rm -rf "${TEMP_DIR}" || (echo "failed to remove ${TEMP_DIR}; exiting." && \
      exit 1)
  fi
fi

mkdir -p "${_basedir}" || (echo "Failed to create ${_basedir}" && exit 1)

git clone "${CONTROL_SOURCE}" "${TEMP_DIR}"

if [ $? -ne 0 ]; then
  echo "Failed to clone ${CONTROL_SOURCE} to ${TEMP_DIR}; exiting"
  exit 1
fi

## Use r10k to fetch all the modules needed
cd "${TEMP_DIR}"
/opt/puppet/bin/r10k Puppetfile install -v || \
  (echo "r10k didn't exit cleanly; exiting" && exit 1)

## Now bootstrap the master
## We just run a puppet apply with the role we have and specify our working
## directory to locate modules
/opt/puppet/bin/puppet apply -e "include ${MASTER_ROLE}" \
  --modulepath="${TEMP_DIR}/modules:${TEMP_DIR}/site:/opt/puppet/share/puppet/modules"

if [ $? -ne 0 ]; then
  echo "The Puppet run didn't exit cleanly.  This might be okay, but you should"
  echo "investigate."
fi
